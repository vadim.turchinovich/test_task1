<?php

class Db{
	
	/* DB Connection Credentials */
	
	private $server = "localhost";
	private $db = "test0";
	private $username = "root";
	private $password = "";
	public $connection;
	
	/* returns mysqli_connection */
	
	public function connect(){
		$this->connection = null;
		try{
			$this->connection = mysqli_connect($this->server, $this->username, $this->password, $this->db);
		}
		catch (mysqli_​sql_​exception $e){
			return $e->getMessage();
		}
		
		return $this->connection;
	}
	
	/* close the connection*/
	public function close(){
		try{
			mysqli_close($this->connection);
		}
		catch (mysqli_sql_exception $e){
			return $e->getMessage();
		}
		return true;
	}
	
	public function SaveUniverse($universe){
		try{
			$query = "INSERT INTO `planets`(`name`, `rotation_period`, `orbital_period`, `diameter`, `climate`, `gravity`, `terrain`, `surface_water`, `population`, `created`, `edited`) VALUES ";
			foreach($universe as $planet){
			$name = mysqli_real_escape_string($planet->name);
			$climate = mysqli_real_escape_string($planet->climate);
			$gravity = mysqli_real_escape_string($planet->gravity);
			$terrain = mysqli_real_escape_string($planet->terrain);
			$query .= "('$name',$planet->rotation_period,$planet->orbital_period,$planet->diameter,'$climate','$gravity','$terrain','$planet->surface_water','$planet->population','$planet->created','$planet->edited'), ";	
			}
			$query =  substr($query, 0, -2); //remove the last comma
			$query .= ";";
			$result = mysqli_query($this->connection, $query);
			return true;
		}
		catch(Exception $e){
			return false;
		}
	}
	/* Save planet object to database */
	public function SavePlanet(\Planet $planet){
		try{
			$name = mysqli_real_escape_string($planet->name);
			$climate = mysqli_real_escape_string($planet->climate);
			$gravity = mysqli_real_escape_string($planet->gravity);
			$terrain = mysqli_real_escape_string($planet->terrain);
			$query = "INSERT INTO `planets`(`planet_id`, `name`, `rotation_period`, `orbital_period`, `diameter`, `climate`, `gravity`, `terrain`, `surface_water`, `population`, `created`, `edited`) VALUES ($planet->planet_id,'$name',$planet->rotation_period,$planet->orbital_period,$planet->diameter,'$climate','$gravity','$terrain','$planet->surface_water','$planet->population','$planet->created','$planet->edited');";
			$result = mysqli_query($this->connection, $query);
			return true;
			}
		catch(mysqli_sql_exception $e){
			
			return false;
		}
	}
	/* Save People object to database */
	public function SavePeople(\People $people){
		try{
			$query = "INSERT IGNORE INTO `people`(`people_id`, `name`, `height`, `mass`, `hair_color`, `skin_color`, `eye_coor`, `birth_year`, `gender`, `created`, `edited`) VALUES ($people->people_id,'$people->name',$people->height,$people->mass,'$people->hair_color','$people->skin_color','$people->eye_color','$people->birth_year','$people->gender','$people->created','$people->edited');";
			$result = mysqli_query($this->connection, $query);
			return true;
			}
		catch(mysqli_sql_exception $e){
			
			return false;
		}
	}
	
	/* Save Planet&People objects link */
	
	public function SaveLink(\Planet $planet, \People $people){
		try{
			$query = "INSERT IGNORE INTO `peopleplanets`(`people_id`, `planet_id`) VALUES($people->people_id, $planet->planet_id);";
			$result = mysqli_query($this->connection, $query);
			return true;	
		}
			catch (mysqli_sql_exception $e){
				echo $e->getMessage();
			return false;
		}
		
	}
	
	
}
?>