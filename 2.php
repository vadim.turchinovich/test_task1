<?php

set_time_limit(0);
date_default_timezone_set('UTC');
require_once 'config/db.php';
require_once 'objects/planet.php';
require_once 'objects/people.php';

/**
 *
 * Fetchs People resource by its url
 *
 * @param   	url 
 * @return      $people
 *
 */
 
function fetchPeople($url){
	$url = $url."?format=json";
	$json_string = file_get_contents(
    $url,
    false,
    stream_context_create(
        array(
            'http' => array(
                'ignore_errors' => true
            )
        )
    )
	);
	
	$json_string = str_replace("unknown", "0", $json_string);
	
	$decoded_array = json_decode($json_string, true);
	
	if(array_key_exists("detail", $decoded_array) && $decoded_array["detail"] === 'Not found'){
		return false;
	}
	
	else{
	$people = new People;
	$id = str_replace('https://swapi.co/api/people/', '', $url);
	$id = str_replace('/', '', $id);
	$id = str_replace('?format=json', '', $id);
	$people->people_id = $id;
	$people->name = $decoded_array['name'];
	$people->height = $decoded_array['height'];
	$people->mass = $decoded_array['mass'];
	$people->hair_color = $decoded_array['hair_color'];
	$people->skin_color = $decoded_array['skin_color'];
	$people->eye_color = $decoded_array['eye_color'];
	$people->birth_year = $decoded_array['birth_year'];
	$people->gender = $decoded_array['gender'];
	$created_timestamp = strtotime($decoded_array['created']);
	$edited_timestamp = strtotime($decoded_array['edited']);
	$people->created = date("Y-m-d H:i:s", $created_timestamp);
	$people->edited = date("Y-m-d H:i:s", $edited_timestamp);
	return $people;
	}
}
/**
 *
 * Fetchs planet by its id
 *
 * @param    planet id
 * @return      Planet
 *
 */
function fetchPlanet($id){
	$json_string = file_get_contents(
    "https://swapi.co/api/planets/$id/?format=json",
    false,
    stream_context_create(
        array(
            'http' => array(
                'ignore_errors' => true
            )
        )
    )
	);
	$json_string = str_replace("unknown", "0", $json_string);
	
	$decoded_array = json_decode($json_string, true);
	if(array_key_exists("detail", $decoded_array) && $decoded_array["detail"] === 'Not found'){
		return false;
	}
	else{
		$planet = new Planet;
		$planet->planet_id = $id;
		$planet->name = $decoded_array['name'];
		$planet->rotation_period = $decoded_array['rotation_period'];
		$planet->orbital_period = $decoded_array['orbital_period'];
		$planet->diameter = $decoded_array['diameter'];
		$planet->climate = $decoded_array['climate'];
		$planet->gravity = $decoded_array['gravity'];
		$planet->terrain = $decoded_array['terrain'];
		$planet->residents = $decoded_array['residents'];
		$planet->surface_water = $decoded_array['surface_water'];
		$planet->population = $decoded_array['population'];
		$created_timestamp = strtotime($decoded_array['created']);
		$edited_timestamp = strtotime($decoded_array['edited']);
		$planet->created = date("Y-m-d H:i:s", $created_timestamp);
		$planet->edited = date("Y-m-d H:i:s", $edited_timestamp);
		return $planet;
	}
}

$Db = new Db;
$Db->connect();
$i = 1;
while(!(fetchPlanet($i) == false))
{
	$planet = fetchPlanet($i);
	foreach($planet->residents as $resident){
		$people = fetchPeople($resident);
		$Db->savePeople($people);
		$Db->saveLink($planet, $people);
	}
	$Db->SavePlanet($planet);
	$i++;
}
$Db->close();

?>