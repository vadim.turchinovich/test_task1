<?php
	class Planet{	
		public $name;
		public $rotation_period;
		public $orbital_period;
		public $diameter;
		public $climate;
		public $gravity;
		public $residents;
		public $terrain;
		public $surface_water;
		public $population;
		public $created;
		public $edited;
	}
?>