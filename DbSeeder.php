<?php

require_once 'config/db.php';
require_once 'objects/planet.php';

if(!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'w'));


function FetchPlanetsPage($page_number){
	$json_string = file_get_contents(
    "https://swapi.co/api/planets/?page=$page_number&format=json",
    false,
    stream_context_create(
        array(
            'http' => array(
                'ignore_errors' => true
            )
        )
    )
	);
	
	$json_string = str_replace("unknown", "0", $json_string);
	
	$decoded_array = json_decode($json_string, true);
	if(array_key_exists("detail", $decoded_array) && $decoded_array["detail"] === 'Not found'){
		return false;
	}
	else{
		foreach ($decoded_array['results'] as $result)
		{
			$planet = new Planet;
			$planet->name = $result['name'];
			$planet->rotation_period = (int)$result['rotation_period'];
			$planet->orbital_period = (int)$result['orbital_period'];
			$planet->diameter = (int)$result['diameter'];
			$planet->climate = $result['climate'];
			$planet->gravity = $result['gravity'];
			$planet->terrain = $result['terrain'];
			$planet->residents = $result['residents'];
			$planet->surface_water = (int)$result['surface_water'];
			$planet->population = (int)$result['population'];
			$created_timestamp = strtotime($result['created']);
			$edited_timestamp = strtotime($result['edited']);
			$planet->created = date("Y-m-d H:i:s", $created_timestamp);
			$planet->edited = date("Y-m-d H:i:s", $edited_timestamp);
			$universe[] = $planet;
		}	
		return $universe;
	}
}

$Db = new Db;
$Db->connect();
$i = 1;
$start_of_fetching_all_data = microtime(true); 
while(!(FetchPlanetsPage($i)== false))
{
	$start_of_fetching_page = microtime(true);
	$universe = FetchPlanetsPage($i);
	$end_of_fetching_page = microtime(true);
	$time = $end_of_fetching_page - $start_of_fetching_page;
	fwrite(STDOUT, "Fetching the pages $i data was $time s.");
	$Db->SaveUniverse($universe);
	$end_of_saving_page = microtime(true);
	$time = $end_of_saving_page - $end_of_fetching_page;
	fwrite(STDOUT, "INSERTING the $i into SQL was $time s.");
	$i++;
}
$end_of_fetching_all_data = microtime(true);

$time = $end_of_fetching_all_data - $start_of_fetching_all_data;

fwrite(STDOUT, "Handling all the data was  $time s.");
$Db->close();

?>