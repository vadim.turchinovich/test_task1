CREATE TABLE IF NOT EXISTS Planets (
	planet_id INT NOT NULL AUTO_INCREMENT,
	name varchar(100),
	rotation_period INT,
	orbital_period INT,
	diameter INT,
	climate varchar(100),
	gravity varchar(100),
	terrain varchar(100),
	surface_water INT,
	population BIGINT,
	created TIMESTAMP,
	edited TIMESTAMP,
	PRIMARY KEY (planet_id)
) ENGINE=INNODB;
